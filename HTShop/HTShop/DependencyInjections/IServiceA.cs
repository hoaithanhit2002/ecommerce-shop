﻿using Microsoft.AspNetCore.Mvc;

namespace HTShop.DependencyInjections
{
    public interface IServiceA 
    {
        string GetId();
    }
}
