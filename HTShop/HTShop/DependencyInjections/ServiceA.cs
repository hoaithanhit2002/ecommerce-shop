﻿namespace HTShop.DependencyInjections
{
    public class ServiceA : IServiceA
    {
        private Guid Id;
        public ServiceA()
        {
            Id = Guid.NewGuid();
        }
        public string GetId()
        {
            return Id.ToString();
        }
    }

    public class ServiceA1 : IServiceA
    {
        private int Id;
        public ServiceA1()
        {
            Id = 5;
        }
        public string GetId()
        {
            return Id.ToString();
        }
    }
}
