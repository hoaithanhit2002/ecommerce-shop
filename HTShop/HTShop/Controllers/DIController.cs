﻿using HTShop.DependencyInjections;
using Microsoft.AspNetCore.Mvc;

namespace HTShop.Controllers
{
    
    public class DIController : Controller
    {
        private readonly IServiceA _serviceA;
        private readonly IServiceA _serviceA1;
        private readonly IServiceA _serviceA2;
        public DIController(IServiceA serviceA, IServiceA serviceA1, IServiceA serviceA2)
        {
            _serviceA = serviceA;
            _serviceA1 = serviceA1;
            _serviceA2 = serviceA2;
            
        }
        public IActionResult Index()
        {
            var idA = _serviceA.GetId();
            var idA1 = _serviceA1.GetId();
            var idA2 = _serviceA2.GetId();

            ViewBag.IdA = idA;
            ViewBag.IdA1 = idA1;
            ViewBag.IdA2 = idA2;
            return View();
        }
    }
}
