﻿using Application.Categories;
using Application.Products;
using Infrastructure.Bills;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
	public static class DependencyInjection
	{
		public static void AddService(this IServiceCollection services)

		{
			services.AddScoped<ICategoryService, CategoryService>();
			services.AddScoped<IProductService, ProductService>();
			services.AddScoped<IBillService, BillService>();
			
		}
	}
}
